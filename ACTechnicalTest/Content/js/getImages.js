﻿$(document).ready(function () {

    var arrImages = [];

    /* Clears they array for next image load*/
    Array.prototype.remove = function (from, to) {
        var rest = this.slice(parseInt(to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    $('#GetImages').click(function (e) {
        e.preventDefault();

        arrImages.remove(0, 5);

        var stockRef = $('#StockRef').val();
        var reg = $('#RegNumber').val();
        var registration = reg.replace(/ /g, '');

        if (!stockRef || !registration) {
            $('#error').show();
        } else {
            $('#error').hide();
        }

        getVehicleList(registration, stockRef);
    })

    $('#GetVehicles').on('click', function () {

        arrImages.remove(0, 5);

        var selected = $('#VehicleList option:selected').text();

        if (selected === "Please select...") {
            $('#error').show();
        } else {
            $('#error').hide();
        }

        var vehicleArray = selected.split(',');

        var make = vehicleArray[0];
        var reg = vehicleArray[1];
        var stock = vehicleArray[2];

        var registration = reg.replace(/ /g, '');
        var stockRef = stock.replace(/ /g, '');

        getVehicleList(registration, stockRef);

    });

    var getVehicleList = function (registration, stockRef) {

        var urlparams = ['f', 'i', 'r', 4, 5, 6];

        var obf_key = stockRef.charAt(0) + registration.charAt(6) + stockRef.charAt(1) + registration.charAt(5) + stockRef.charAt(2) + registration.charAt(4) + stockRef.charAt(3) + registration.charAt(3) + stockRef.charAt(4) + registration.charAt(2) + stockRef.charAt(5) + registration.charAt(1) + stockRef.charAt(6) + registration.charAt(0) + stockRef.charAt(8);

        for (i = 0; i < urlparams.length; i++) {
            var camera = urlparams[i];
            var urlSmallImages = '<span><img class="vehicleImg" src="http://vcache.arnoldclark.com/imageserver/' + obf_key + '/350/' + camera + '" /></span>';

            arrImages.push(urlSmallImages);
        }

        $('#results').html('<span id="images">' + arrImages + '</span>');
    }
});